namespace DepotTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equipments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        PartNumber = c.String(),
                        SerialNumber = c.String(),
                        Name = c.String(),
                        RMA_model_UnitID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RMA_Model", t => t.RMA_model_UnitID)
                .Index(t => t.RMA_model_UnitID);
            
            CreateTable(
                "dbo.RMA_Model",
                c => new
                    {
                        UnitID = c.Int(nullable: false),
                        ID = c.Int(nullable: false),
                        dateOpen = c.DateTime(nullable: false),
                        dateUnitShipped = c.DateTime(nullable: false),
                        dateDepotRecieved = c.DateTime(nullable: false),
                        dateDepotShipped = c.DateTime(nullable: false),
                        dateUnitRecieved = c.DateTime(nullable: false),
                        EquipmentID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UnitID)
                .ForeignKey("dbo.Units", t => t.UnitID)
                .Index(t => t.UnitID);
            
            CreateTable(
                "dbo.Units",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PointOfContactName = c.String(),
                        PointOfContactPhoneNumber = c.String(),
                        Custodian = c.String(),
                        CustodianContactPhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RMA_Model", "UnitID", "dbo.Units");
            DropForeignKey("dbo.Equipments", "RMA_model_UnitID", "dbo.RMA_Model");
            DropIndex("dbo.RMA_Model", new[] { "UnitID" });
            DropIndex("dbo.Equipments", new[] { "RMA_model_UnitID" });
            DropTable("dbo.Units");
            DropTable("dbo.RMA_Model");
            DropTable("dbo.Equipments");
        }
    }
}
