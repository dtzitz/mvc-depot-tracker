namespace DepotTracker.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRequiredValues : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.RMA_Model", "dateOpen", c => c.DateTime());
            AlterColumn("dbo.RMA_Model", "dateUnitShipped", c => c.DateTime());
            AlterColumn("dbo.RMA_Model", "dateDepotRecieved", c => c.DateTime());
            AlterColumn("dbo.RMA_Model", "dateDepotShipped", c => c.DateTime());
            AlterColumn("dbo.RMA_Model", "dateUnitRecieved", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.RMA_Model", "dateUnitRecieved", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RMA_Model", "dateDepotShipped", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RMA_Model", "dateDepotRecieved", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RMA_Model", "dateUnitShipped", c => c.DateTime(nullable: false));
            AlterColumn("dbo.RMA_Model", "dateOpen", c => c.DateTime(nullable: false));
        }
    }
}
