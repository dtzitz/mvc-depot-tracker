﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DepotTracker.Models;
using System.Data.Entity;

namespace DepotTracker.DAL
{
    public class DepotContext: DbContext
    {
        public DepotContext() : base("DepotContext")
        {

        }
        public DbSet<RMA_Model> RMA_Model { get; set; }
        public DbSet<Equipment> Equipment { get; set; }
        public DbSet<Unit> Unit { get; set; }
    }
}