﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DepotTracker.Startup))]
namespace DepotTracker
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
