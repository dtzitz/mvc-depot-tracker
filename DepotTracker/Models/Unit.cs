﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DepotTracker.Models
{
    public class Unit
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string PointOfContactName { get; set; }
        public string PointOfContactPhoneNumber { get; set; }
        public string Custodian { get; set; }
        public string CustodianContactPhoneNumber { get; set; }

        
        public virtual RMA_Model RMA_model { get; set; }
        
       
    }
}