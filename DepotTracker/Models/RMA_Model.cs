﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DepotTracker.Models
{
    public class RMA_Model
    {
        public int ID { get; set; }

        public DateTime? dateOpen { get; set; }
        public DateTime? dateUnitShipped { get; set; }
        public DateTime? dateDepotRecieved { get; set; }
        public DateTime? dateDepotShipped { get; set; }
        public DateTime? dateUnitRecieved { get; set; }

        //foreign keys
        [Key, ForeignKey("Unit")]
        public int UnitID { get; set; }
        public int EquipmentID { get; set; }
        
        
        public virtual ICollection<Equipment> Equipment { get; set; }
        public virtual Unit Unit { get; set; }
        


    }
}