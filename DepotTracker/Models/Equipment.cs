﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DepotTracker.Models
{
    public class Equipment
    {

        
        public int ID { get; set; }

        public string PartNumber { get; set; }
        public string SerialNumber { get; set; }
        public string Name { get; set; }

        public virtual RMA_Model RMA_model { get; set; }

        
    }
}