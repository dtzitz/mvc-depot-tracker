﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DepotTracker.DAL;
using DepotTracker.Models;

namespace DepotTracker.Controllers
{
    public class RMA_ModelController : Controller
    {
        private DepotContext db = new DepotContext();

        // GET: RMA_Model
        public ActionResult Index()
        {
            var rMA_Model = db.RMA_Model.Include(r => r.Unit);
            return View(rMA_Model.ToList());
        }

        // GET: RMA_Model/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RMA_Model rMA_Model = db.RMA_Model.Find(id);
            if (rMA_Model == null)
            {
                return HttpNotFound();
            }
            return View(rMA_Model);
        }

        // GET: RMA_Model/Create
        public ActionResult Create()
        {
            ViewBag.UnitID = new SelectList(db.Unit, "ID", "Name");
            return View();
        }

        // POST: RMA_Model/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UnitID,ID,dateOpen,dateUnitShipped,dateDepotRecieved,dateDepotShipped,dateUnitRecieved,EquipmentID")] RMA_Model rMA_Model)
        {
            if (ModelState.IsValid)
            {
                db.RMA_Model.Add(rMA_Model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UnitID = new SelectList(db.Unit, "ID", "Name", rMA_Model.UnitID);
            return View(rMA_Model);
        }

        // GET: RMA_Model/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RMA_Model rMA_Model = db.RMA_Model.Find(id);
            if (rMA_Model == null)
            {
                return HttpNotFound();
            }
            ViewBag.UnitID = new SelectList(db.Unit, "ID", "Name", rMA_Model.UnitID);
            return View(rMA_Model);
        }

        // POST: RMA_Model/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UnitID,ID,dateOpen,dateUnitShipped,dateDepotRecieved,dateDepotShipped,dateUnitRecieved,EquipmentID")] RMA_Model rMA_Model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(rMA_Model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UnitID = new SelectList(db.Unit, "ID", "Name", rMA_Model.UnitID);
            return View(rMA_Model);
        }

        // GET: RMA_Model/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RMA_Model rMA_Model = db.RMA_Model.Find(id);
            if (rMA_Model == null)
            {
                return HttpNotFound();
            }
            return View(rMA_Model);
        }

        // POST: RMA_Model/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RMA_Model rMA_Model = db.RMA_Model.Find(id);
            db.RMA_Model.Remove(rMA_Model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
